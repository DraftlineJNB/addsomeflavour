<div class="new-registration-form-template">
<div class="row">
	<div class="star-one">
		<img src="/sites/g/files/phfypu1826/f/stars-two.png" alt="">
	</div>
	<div class="star-two">
		<img src="/sites/g/files/phfypu1826/f/stars-two.png" alt="">
	</div>
	<div class="star-three">
		<img src="/sites/g/files/phfypu1826/f/stars-two.png" alt="">
	</div>
	<div class="food-before-form">
		<img src="/sites/g/files/phfypu1826/f/TRY-IT-TODAY-con.png" alt="">
	</div>
	<div class="food-before-form-refreshing">
		<img src="/sites/g/files/phfypu1826/f/REFRESHING%21%21%21.png" alt="">
	</div>
	<div class="food-before-form-yum">
		<img src="/sites/g/files/phfypu1826/f/YUM%21-blue.png" alt="">
	</div>
	<div class="form-header">
		<div class="col-md-6 col-xs-12">
			<strong><?php print drupal_render($form['submitted']['contact_us_title']); ?></strong>
			<p>Telephone: 0860 12 14 14 </p>
			<p>Customer services respond within 7 days.</p>
			<br>
		</div>
	</div>
	<div class="name-surname">
		<div class="col-md-6 col-xs-12 w-first">
			<?php print drupal_render($form['submitted']['name']); ?>
		</div>
		<div class="col-md-6 col-xs-12 w-first">
			<?php print drupal_render($form['submitted']['surname']); ?>
		</div>
	</div>
	<div class="contact-details">
		<div class="col-md-6 col-xs-12 w-second">
			<?php print drupal_render($form['submitted']['email']); ?>
		</div>
		<div class="col-md-6 col-xs-12 w-second">
			<?php print drupal_render($form['submitted']['cellphone_number']); ?>
		</div>
	</div>
	<div class="col-md-6 col-xs-12 your-message">
		<?php print drupal_render($form['submitted']['your_message']); ?>
	</div>
	<div class="food-after-form">
		<img src="/sites/g/files/phfypu1826/f/Hands-l.png" alt="">
	</div>
	<div class="food-after-form-yum">
		<img src="/sites/g/files/phfypu1826/f/YUM%21.png" alt="">
	</div>
	<div class="food-after-form-unique">
		<img src="/sites/g/files/phfypu1826/f/UNIQUELY-UNIQUE.png" alt="">
	</div>
</div>

<div class="row form-group">
	<div class="col-md-6 col-xs-12">
		<div class="row dob-select-group">
			<div class="col day-select">
			<?php print drupal_render($form['submitted']['date_of_birth']['day']); ?>
			</div>

			<div class="col month-select">
			<?php print drupal_render($form['submitted']['date_of_birth']['month']); ?>
			</div>

			<div class="col year-select">
			<?php print drupal_render($form['submitted']['date_of_birth']['year']); ?>
			</div>	
		</div>
	</div>
	<div class="col-md-6 col-xs-12 w-first">
		<?php print drupal_render($form['submitted']['gender']); ?>
	</div>
</div>

<div class="row">
	<div class="col-md-6 col-xs-12 w-first">
		<?php print drupal_render($form['submitted']['email']); ?>
	</div>
	<div class="col-md-6 col-xs-12 w-first">
		<?php print drupal_render($form['submitted']['cellphone_number']); ?>
	</div>
</div>

<div class="row">
	
	<div class="col-md-6 col-xs-12 w-first">
		<?php print drupal_render($form['submitted']['province']); ?>
	</div>

	<div class="col-md-6 col-xs-12 w-first">
		<?php print drupal_render($form['submitted']['town']); ?>
	</div>
</div>

<div class="row">
	<div class="col what-you-like ">
		<?php print drupal_render($form['submitted']['tell_us_what_you_like']); ?>
	</div>
</div>

<div class="row">
	<div class="col ">
		<?php print drupal_render($form['submitted']['what_are_you_interested_in']); ?>
	</div>
</div>

<div class="row">
	<div class="col-md-4 col-xs-12 w-first">
		<?php print drupal_render($form['submitted']['fashion']); ?>
	</div>
	<div class="col-md-4 col-xs-12 w-first">
		<?php print drupal_render($form['submitted']['food']); ?>
	</div>
	<div class="col-md-4 col-xs-12 w-first">
		<?php print drupal_render($form['submitted']['music']); ?>
	</div>
</div>

<div class="row">
	<div class="col ">
		<?php print drupal_render($form['submitted']['drink_your_flying_fish']); ?>
	</div>
</div>

<div class="row">
	<div class="col-md-4 mt-4 col-xs-12 w-first drink-flying-fish-with">
		<?php print drupal_render($form['submitted']['drink_your_flying_fish_with']); ?>
	</div>
</div>

<div class="condition-submit-section">
	<div class="conditions-section">
		<div class="row">
			<div class="col mt-4">
				<input type="radio" id="terms" name="submitted[terms]" value="yes" required>
				<label for="terms">I accept the <a href="https://www.addsomeflavour.co.za/sites/g/files/phfypu1231/f/flying-fish-flyday-drops.pdf"> Terms & Conditions </a> and the Privacy Policy </label>
				</div>
		</div>

		<div class="row mt-2">
			
			<div class="col">
				<input type="radio" id="events" name="submitted[subscribe]" value="yes">
				<label for="events">Please send me information on future events and promotions</label>
			</div>
		</div>
	</div>

	<div class="sumbit-section">
		<div class="col-md-12 text-center">
			<button class="mx-auto">REGISTER</button>
		</div>
	</div>
</div>
</div>

<div class="d-none">
	<?php print drupal_render_children($form); ?>
</div>


			
