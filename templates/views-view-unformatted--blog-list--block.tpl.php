<?php

/**
 * @file
 * Default simple view template to display a list of rows.
 *
 * @ingroup views_templates
 */
$rows_slices = array();
for ($i = 0; $i < count($rows); $i += 6) {
  $slice = array_slice($rows, $i, 6);
  $rows_slices[] = $slice;
}
?>
<?php if (!empty($title)): ?>
    <h3><?php print $title; ?></h3>
  <?php endif; ?>
  
  <?php foreach ($rows_slices as $key => $rowSlice): ?>
    <div class="container-blog-slideshow">
  <?php foreach ($rowSlice as $id => $row): ?>
      <div<?php if ($classes_array[$id]): ?> class="<?php print $classes_array[$id]; ?>"<?php endif; ?>>
      <?php print $row; ?>
      </div>
  <?php endforeach; ?>
    </div>
<?php endforeach; ?>
