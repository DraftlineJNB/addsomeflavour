<?php
	/**
	 * 
	 * Developer: Lebogang Selema
	 * Brand: Flying Fish
	 * Project: Flow Room
	 * Owner: AbInBev
	 * Date: 18 Aug 2021
	 * 
	 */
?>

<div class="flow-room-registration">
	<div class="flow-room-smeg">
		<img src="/sites/g/files/phfypu1826/f/mealbootomimageflowroom.png">
	</div>
	<div class="flow-room-webform">
		<?php print render(node_view(node_load(331), 'full', NULL));?>
	</div>
</div>
