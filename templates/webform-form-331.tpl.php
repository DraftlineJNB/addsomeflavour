<div class="new-registration-form-template">
<div class="row flow-room-header">
	<h1>SIGN UP TO</h1>
	<h1>THE FLOW ROOM</h1>
	<p>SIGN UP TO STAND A CHANCE TO WIN AMAZING PRIZES.</p>
</div>
<div class="row">
	<div class="col-md-6 col-xs-12 w-first">
		<?php print drupal_render($form['submitted']['name']); ?>
	</div>
	<div class="col-md-6 col-xs-12 w-first">
		<?php print drupal_render($form['submitted']['surname']); ?>
	</div>
</div>

<div class="row form-group">
	<div class="col-md-6 col-xs-12">
		<div class="row dob-select-group">
			<div class="col day-select">
			<?php print drupal_render($form['submitted']['date_of_birth']['day']); ?>
			</div>

			<div class="col month-select">
			<?php print drupal_render($form['submitted']['date_of_birth']['month']); ?>
			</div>

			<div class="col year-select">
			<?php print drupal_render($form['submitted']['date_of_birth']['year']); ?>
			</div>	
		</div>
	</div>
	<div class="col-md-6 col-xs-12 w-first">
		<?php print drupal_render($form['submitted']['gender']); ?>
	</div>
</div>

<div class="row">
	<div class="col-md-6 col-xs-12 w-first">
		<?php print drupal_render($form['submitted']['email']); ?>
	</div>
	<div class="col-md-6 col-xs-12 w-first">
		<?php print drupal_render($form['submitted']['cellphone_number']); ?>
	</div>
</div>

<div class="row">
	
	<div class="col-md-6 col-xs-12 w-first">
		<?php print drupal_render($form['submitted']['province']); ?>
	</div>

	<div class="col-md-6 col-xs-12 w-first">
		<?php print drupal_render($form['submitted']['town']); ?>
	</div>
</div>

<div class="row">
	<div class="col what-you-like ">
		<?php print drupal_render($form['submitted']['tell_us_what_you_like']); ?>
	</div>
</div>

<div class="row">
	<div class="col ">
		<?php print drupal_render($form['submitted']['what_are_you_interested_in']); ?>
	</div>
</div>

<div class="row">
	<div class="col-md-4 col-xs-12 w-first">
		<?php print drupal_render($form['submitted']['fashion']); ?>
	</div>
	<div class="col-md-4 col-xs-12 w-first">
		<?php print drupal_render($form['submitted']['food']); ?>
	</div>
	<div class="col-md-4 col-xs-12 w-first">
		<?php print drupal_render($form['submitted']['music']); ?>
	</div>
</div>

<div class="row">
	<div class="col ">
		<?php print drupal_render($form['submitted']['drink_your_flying_fish']); ?>
	</div>
</div>

<div class="row">
	<div class="col-md-4 mt-4 col-xs-12 w-first drink-flying-fish-with">
		<?php print drupal_render($form['submitted']['drink_your_flying_fish_with']); ?>
	</div>
</div>

<div class="row">
	<div class="col mt-4">
		<?php print drupal_render($form['submitted']['terms_and_conditions']); ?>
	</div>
	
</div>

<div class="row mt-2">
	<div class="col">
		<?php print drupal_render($form['submitted']['subscribe']); ?>
	</div>
</div>

<div class="col-md-12 text-center">
	<button class="mx-auto">Submit</button>
</div>
</div>

<div class="d-none">
	<?php print drupal_render_children($form); ?>
</div>

<script>
	console.log("Starting flow auto play");
	if (jQuery("body").hasClass("flow-room-drops")) {
		jQuery( ".drop-it-video .icon-image" ).click(function() {
			jQuery('.drop-it-video .icon-image').css('display','none');
			jQuery('.drop-it-video .background-image').css('display','none');
			jQuery("#drop-it-video-play")[0].src += "&autoplay=1";
		});
	}
</script>