function makeSubmenu(value) {

      var citiesByState = {
            ec: ["Alice", "Butterworth", "East London", "Graaff-Reinet", "Grahamstown", "King William’s Town", "Mthatha", "Port Elizabeth", "Queenstown", "Uitenhage", "ZwelitshaBhubaneswar"],
            fs: ["Bethlehem","Bloemfontein","Jagersfontein","Kroonstad","Odendaalsrus","Parys","Phuthaditjhaba","Sasolburg","Virginia","Welkom"],
            gauteng: ["Benoni","Boksburg","Brakpan","Carletonville","Germiston","Johannesburg","Krugersdorp","Pretoria","Randburg","Randfontein","Roodepoort","Soweto","Springs","Vanderbijlpark","Vereeniging"],
            kzn: ["Durban","Empangeni","Ladysmith","Newcastle","Pietermaritzburg","Pinetown","UlundiUmlazi"],
            limpopo: ["Giyani","Lebowakgomo","Musina","Phalaborwa","Polokwane","Seshego","Sibasa","Thabazimbi"],
            mpumalanga: ["Emalahleni","Nelspruit","Secunda"],
            nw: ["Klerksdorp","Mahikeng","Mmabatho","Potchefstroom","Rustenburg"],
            wc: ["Bellville" ,"Cape Town" ,"Constantia" ,"George" ,"Hopefield" ,"Oudtshoorn" ,"Paarl", "Simon’s Town" ,"Stellenbosch" ,"Swellendam" ,"Worcester"],
            nc: ["Kimberley", "Kuruman", "Port Nolloth"]
      }
      
      if(value.length==0) document.getElementById("edit-submitted-town").innerHTML = "<option></option>";
      else {
            var citiesOptions = "";
            if(citiesByState[value]) {
                  for( cityId in citiesByState[value]) {
                        citiesOptions+="<option value='"+citiesByState[value][cityId]+"'>"+citiesByState[value][cityId]+"</option>";
                  }
                  document.getElementById("edit-submitted-town").innerHTML = citiesOptions;
            }
      }
}