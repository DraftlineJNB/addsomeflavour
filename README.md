# README #

This README contains CDP vendors documentation.

## CDP INTEGRATION ##

#### Personal Information of the responsable of the CDP Implementation   ####

* __Company__        :   VML&YR


#### Version of Post Back API Implemented ####

* postback-api-1.2

#### Details of the Module Used ####

* __Name__                               :   ab_user_information_storage
* __File Route__                         :   /sites/all/modules/custom/ab_user_information_storage
* __Details and Features of the module__ :
    * _Custom module name_        : ab_user_information_storage
    * CMS Administrative Variables:
        * ab_user_information_storage_variable_webform_enable
        * ab_user_information_storage_variable_webform_debug
        * ab_user_information_storage_variable_webform_env
        * ab_user_information_storage_variable_zone
        * ab_user_information_storage_variable_brand
        * ab_user_information_storage_variable_campaign
    * Main curl function          :
        * curl_init
        * curl_setopt_array
        * curl_exec
        * curl_getinfo
        * curl_error

#### _abi_campaing_ ####
* Flyingfish_main
#### _abi_brand_ ####
* Flying Fish
#### _abi_form_ ####
* flyingfish_register
#### _Country_ ####
* zaf
#### Details of the Connected Form ####
* Form URL                                    : /register-events
* Type of form used                           : Webform
* Where does the data is being stored locally*: Webform Submissions
#### Aditional Comments ####
*NONE*
