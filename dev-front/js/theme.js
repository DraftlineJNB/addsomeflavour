

jQuery(document).ready(function ($) {
  window.twttr = (function (d, s, id) {
    var js,
      fjs = d.getElementsByTagName(s)[0],
      t = window.twttr || {};
    if (d.getElementById(id)) return t;
    js = d.createElement(s);
    js.id = id;
    js.src = "https://platform.twitter.com/widgets.js";
    fjs.parentNode.insertBefore(js, fjs);

    t._e = [];
    t.ready = function (f) {
      t._e.push(f);
    };

    return t;
  })(document, "script", "twitter-wjs");

  // -- General -- //
  const $GeneralScope = {
    // Constructor
    init: function () {
      this.bgScripts();
      this.menuScripts();
      this.navigationSnippet();
    },

    // Background sections
    bgScripts: function () {
      let BackgroundDesktop = $("body").data("bg-desktop");
      let BackgroundMobile = $("body").data("bg-mobile");

      if (BackgroundDesktop || BackgroundMobile) {
        $(window).on("resize load", function (event) {
          event.preventDefault();
          /* Act on the event */
          if ($(window).width() > 768) {
            $("body").css("background-image", "url(" + BackgroundDesktop + ")");
          } else {
            $("body").css("background-image", "url(" + BackgroundMobile + ")");
          }
        });
      }
    },

    // Menu scripts
    menuScripts: function () {
      let ButtonTrigger = $(".navbar-header .navbar-toggle");
      let MenuWrapper = $(".navbar-header .navbar-collapse");

      if (ButtonTrigger) {
        $(document).on("click", ".navbar-toggle", function () {
          ButtonTrigger.toggleClass("collapsed");
        });
      }
    },

    navigationSnippet: function () {
      $('a[href*="#"]')
        // Remove links that don't actually link to anything
        .not('[href="#"]')
        .not('[href="#0"]')
        .click(function (event) {
          // On-page links
          if (
            location.pathname.replace(/^\//, "") ==
            this.pathname.replace(/^\//, "") &&
            location.hostname == this.hostname
          ) {
            // Figure out element to scroll to
            let target = $(this.hash);

            target = target.length
              ? target
              : $("[name=" + this.hash.slice(1) + "]");
            // Does a scroll target exist?
            if (target.length) {
              // Only prevent default if animation is actually gonna happen
              event.preventDefault();
              $("html, body").animate(
                {
                  scrollTop: target.offset().top
                },
                1000,
                function () {
                  // Callback after animation
                  // Must change focus!
                  let $target = $(target);
                  //$target.focus();

                  if ($target.is(":focus")) {
                    // Checking if the target was focused
                    return false;
                  } else {
                    $target.attr("tabindex", "-1"); // Adding tabindex for elements not focusable
                    //$target.focus(); // Set focus again
                  }
                }
              );
            }
          }
        });

      $('.snickers').slick({
        infinite: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        dots: true

      });
    }
  };

  // -- Agegate -- //
  const $AgegateScope = {
    // Constructor
    init: function () {
      this.ageScripts();
    },
    // scripts for Agegate
    ageScripts: function () {
      // Dom manipulation
      let ListCountries = $(".form-item-list-of-countries");
      let Checklist = $(".form-item-remember-me");
      let FbValidate = $(".age_checker_facebook_validate");
      let RememberMe = $(".ab-inbev-remember-me");
      let RememberLabel = Checklist.find("label");
      let RememberMeStr = RememberMe.find("strong");

      if (ListCountries) {
        ListCountries.insertAfter("#age_checker_error_message");
      }

      if (Checklist) {
        Checklist.append(RememberMe);
        RememberLabel.append(RememberMeStr);
      }

      if (FbValidate) {
        FbValidate.insertAfter("#edit-submit");
        $('<div class="connector">OR</div>').insertBefore(FbValidate);
        FbValidate.append(
          '<span class="fbTrigger">Sign in with facebook</span>'
        );
      }
    }
  };

  // -- Home -- //
  const $HomeScope = {
    // Constructor
    init: function () {
      this.eventsScripts();
      this.animation();
      this.twiterWiget();
      this.productSlider();
      this.twitterSlider();

    },

    productSlider: function () {
      let slider_wrapper = $(".content-featured");
      let slider_items = slider_wrapper.find(".container-blog-slideshow");
      let slick_settings = {
        arrows: false,
        dots: true,
        slidesToShow: 2,
        slidesToScroll: 1,
        responsive: [
          {
            breakpoint: 9999,
            settings: "unslick"
          },
          {
            breakpoint: 767,
            settings: {
              arrows: false,
              dots: true,
              slidesToShow: 2,
              slidesToScroll: 1,
            }
          }
        ]
      };

      if (!slider_wrapper.hasClass("slick-initialized")) {
        const slick_slider = slider_wrapper.slick(slick_settings);
      }
    },

    twitterSlider: function () {
      let slider_wrapper = $("#timeline");
      // let slider_items = slider_wrapper.find(".container-blog-slideshow");
      let slick_settings = {
        arrows: false,
        dots: false,
        infinite: false,
        slidesToShow: 1,
        slidesToScroll: 1,
        centerMode: true,
        centerPadding: "14px",
        responsive: [
          {
            breakpoint: 9999,
            settings: "unslick"
          },
          {
            breakpoint: 767,
            settings: {
              arrows: false,
              dots: false,
              infinite: false,
              slidesToShow: 1,
              slidesToScroll: 1,
              centerMode: true,
              centerPadding: "14px",
            }
          }
        ]
      };

      if (!slider_wrapper.hasClass("slick-initialized")) {
        const slick_slider = slider_wrapper.slick(slick_settings);
      }
    },

    twiterWiget: function () {
      TweetJs.ListTweetsOnUserTimeline("@FlyingFishSA", function (data) {
        var json = data;
        var counter = 0;
        var baseWidth = 300;
        $(function () {
          var elem = document.querySelector("#container2");

          twttr.ready(function (twttr) {
            rayoutTimeline().then(function () {
              jQuery("#container2").masonry({
                itemSelector: ".grid-item",
                columnWidth: ".grid-sizer",
                gutter: 10
              });
            });
          });
        });

        function rayoutTimeline() {
          var d = new $.Deferred();
          var $timeline = $("#timeline");

          $.each(json, function (i, item) {
            if (i < 8) {
              var $grid = $('<div class="grid-item">');
              $timeline.append($grid);
              $grid.attr("data-index", i);
              twttr.widgets
                .createTweet(item.in_reply_to_status_id_str, $grid.get(0), {
                  align: "left",
                  width: baseWidth,
                  //cards: 'hidden',
                  conversation: "none"
                })
                .then(function (el) {
                  var $el = $(el);
                  var $pl = $el.parent();
                  var index = parseInt($pl.attr("data-index"));
                  $el.css({ opacity: 1, "transition-delay": index / 10 + "s" });
                  counter++;
                  if (counter === json.length) {
                    console.log(counter);
                    return d.resolve();
                  }
                });
            }
          });

          return d.promise();
        }
      });
    },

    animation: function () {
      var s = skrollr.init({
        smoothScrolling: true,
        scale: 1,
        easing: {
          WTF: Math.random,
          inverted: function (p) {
            return 1 - p;
          }
        }
      });
    },

    // scripts for Agegate
    eventsScripts: function () {
      let ViewWrapper = $(".view-upcoming-events-home .view-content");
      let ViewItems = ViewWrapper.find(".views-row");

      if (ViewItems) {
        ViewItems.each(function (index, el) {
          let Instance = $(this);
          let ImageSource = Instance.find(
            ".home-upcoming-events-image img"
          ).attr("src");

          if (ImageSource) {
            Instance.css("background-image", "url(" + ImageSource + ")");
          }
        });
      }
    }
  };

  // -- Home -- //
  const $WtfScope = {
    // Constructor
    init: function () {
      this.twiterWiget();
      this.twitterSlider();
    },


    twitterSlider: function () {
      let slider_wrapper = $("#timeline");
      // let slider_items = slider_wrapper.find(".container-blog-slideshow");
      let slick_settings = {
        arrows: false,
        dots: false,
        infinite: false,
        slidesToShow: 1,
        slidesToScroll: 1,
        centerMode: true,
        centerPadding: "14px",
        responsive: [
          {
            breakpoint: 9999,
            settings: "unslick"
          },
          {
            breakpoint: 767,
            settings: {
              arrows: false,
              dots: false,
              infinite: false,
              slidesToShow: 1,
              slidesToScroll: 1,
              centerMode: true,
              centerPadding: "14px",
            }
          }
        ]
      };

      if (!slider_wrapper.hasClass("slick-initialized")) {
        const slick_slider = slider_wrapper.slick(slick_settings);
      }
    },

    twiterWiget: function () {
      TweetJs.ListTweetsOnUserTimeline("@FlyingFishSA", function (data) {
        var json = data;
        var counter = 0;
        var baseWidth = 300;
        $(function () {
          var elem = document.querySelector("#container2");

          twttr.ready(function (twttr) {
            rayoutTimeline().then(function () {
              jQuery("#container2").masonry({
                itemSelector: ".grid-item",
                columnWidth: ".grid-sizer",
                gutter: 10
              });
            });
          });
        });

        function rayoutTimeline() {
          var d = new $.Deferred();
          var $timeline = $("#timeline");

          $.each(json, function (i, item) {
            if (i < 8) {
              var $grid = $('<div class="grid-item">');
              $timeline.append($grid);
              $grid.attr("data-index", i);
              twttr.widgets
                .createTweet(item.in_reply_to_status_id_str, $grid.get(0), {
                  align: "left",
                  width: baseWidth,
                  //cards: 'hidden',
                  conversation: "none"
                })
                .then(function (el) {
                  var $el = $(el);
                  var $pl = $el.parent();
                  var index = parseInt($pl.attr("data-index"));
                  $el.css({ opacity: 1, "transition-delay": index / 10 + "s" });
                  counter++;
                  if (counter === json.length) {
                    console.log(counter);
                    return d.resolve();
                  }
                });
            }
          });

          return d.promise();
        }
      });
    }


  };

  // -- About -- //
  const $AboutScope = {
    // Constructor
    init: function () {
      this.aboutTimeLineScripts();
      this.tabScript();
      this.flavourSlider();
    },
    // scripts for Agegate
    tabScript: function () {
      $('.views-row .tab-buttons a:first-of-type').addClass('active');

      let tabItems = $('.tab-buttons a');
      tabItems.click(function (e) {
        e.preventDefault();
        let tabParent = $(this).parents('.content-tabs')

        $(this).addClass('active');
        tabParent.find(tabItems).not(this).removeClass('active');
        // tabItems.not(this).removeClass('active');

        if ($(this).hasClass('link-flavour')) {
          tabParent.find('.tab-flavour').show();
          tabParent.children().not('.tab-flavour, .tab-buttons').hide();
        } else {
          tabParent.find('.tab-nutrition').show();
          tabParent.children().not('.tab-nutrition, .tab-buttons').hide();
        }

      });

      var firstItem = $('.content-select div[data-content]:first-of-type');
      for (var a = 0; a < firstItem.length; a++) {
        var selectData = $(firstItem[a]).attr('data-content');
        $(firstItem[a]).parents('.tab-nutrition').find('.custom-options li[data-value="' + selectData + '"]').addClass('selected');
        $(firstItem[a]).parents('.tab-nutrition').find('.custom-select__trigger span').text(selectData)
      }
      // $('.custom-options li:first-of-type').addClass('selected');

      $('.custom-select').click(function () {
        $(this).toggleClass('open');
      });

      var options = $(".custom-options li");

      for (var i = 0; i < options.length; i++) {
        $(options[i]).click(function () {
          if (!$(this).hasClass('selected')) {
            var dataOption = $(this).attr('data-value');

            $(this).parents('.tab-nutrition').find('.content-select div[data-content]').not('.content-select div[data-content="' + dataOption + '"]').hide();
            // $('.content-select div').not('.content-select div[data-content="' + dataOption +'"]').hide();
            $(this).parents('.tab-nutrition').find('.content-select div[data-content="' + dataOption + '"]').show();

            $(this).addClass('selected');
            $('.custom-options li').not(this).removeClass('selected');

            $(this).closest('.custom-select').find('.custom-select__trigger span').text(dataOption);
          }

        });
      }

    },
    flavourSlider: function () {
      let slider_wrapper = $(".view-our-flavour .view-content");
      // let slider_items = slider_wrapper.find(".container-blog-slideshow");
      let slick_settings = {
        arrows: true,
        //R
        dots: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        infinite: false,
        adaptiveHeight: true
      };

      if (!slider_wrapper.hasClass("slick-initialized")) {
        const slick_slider = slider_wrapper.slick(slick_settings);
      }


    },


    aboutTimeLineScripts: function () {
      let slider_wrapper = $(".view-about .view-content");
      let slider_items = slider_wrapper.find(".views-row");
      let slick_settings = {
        arrows: false,
        dots: true,
        slidesToShow: 3,
        slidesToScroll: 1,
        vertical: true,
        verticalSwiping: true,
        responsive: [
          {
            breakpoint: 1028,
            settings: {
              speed: 300,
              slidesToScroll: 1,
              slidesToShow: 1,
              vertical: false,
              verticalSwiping: false,
              arrows: true,
              dots: false
            }
          }
        ]
      };

      if (!slider_wrapper.hasClass("slick-initialized")) {
        const slick_slider = slider_wrapper.slick(slick_settings);
      }

      if (slider_items) {
        slider_items.each(function (index, el) {
          let instance = $(this);
          let titleItem = instance.find(".icon-pager");
          let titleColor = titleItem.data("color");

          if (instance.hasClass("slick-current")) {
            // Add color in span icon
            titleItem.css("background-color", "#" + titleColor);
            instance.find("h4").css("color", "#" + titleColor);
          } else {
            titleItem.css("background-color", "#002f73");
          }

          // Actice click
          if ($(window).width() > 768) {
            instance.click(function (event) {
              /* Act on the event */
              slider_items.removeClass("slick-current");
              instance.addClass("slick-current");

              slider_items
                .find(".icon-pager")
                .css("background-color", "#002f73");

              if (instance.hasClass("slick-current")) {
                // Add color in span icon
                titleItem.css("background-color", "#" + titleColor);
                instance.find("h4").css("color", "#" + titleColor);
              }
            });
          }
        });
      }
    }
  };

  // -- Events -- //
  const $EventsScope = {
    // Constructor
    init: function () {
      this.eventListScripts();
    },
    eventListScripts: function () {
      let ViewWrapper = $(".view-events .view-content");
      let ViewItems = ViewWrapper.find(".views-row");
      let slick_settings = {
        arrows: true,
        dots: false,
        slidesToShow: 3,
        slidesToScroll: 1,
        centerMode: true,
        variableWidth: true,
        responsive: [
          {
            breakpoint: 1025,
            settings: {
              slidesToScroll: 1,
              slidesToShow: 1,
              centerMode: false,
              variableWidth: false
            }
          }
        ]
      };

      if (ViewItems) {
        ViewItems.each(function (index, el) {
          let Instance = $(this);
          let ImageSource = Instance.find(".page-event-image img").attr("src");

          if (ImageSource) {
            Instance.css("background-image", "url(" + ImageSource + ")");
          }
        });

        if (!ViewWrapper.hasClass("slick-initialized")) {
          const slick_slider = ViewWrapper.slick(slick_settings);
        }
      }
    }
  };

  // -- WhatsApp Friends -- //
  const $WhatsScope = {
    // Constructor
    init: function () {
      this.colorTitleScript();
    },
    colorTitleScript: function () {
      let ViewWrapper = $(".view-whatsapp-steps .view-content");
      let ViewItems = ViewWrapper.find(".views-row");

      if (ViewItems) {
        ViewItems.each(function (index, el) {
          let instance = $(this);
          let colorTitle = instance.find(".page-whatsapp-item").data("color");
          let titleDom = instance.find("h3");

          titleDom.css("color", "#" + colorTitle);
        });
      }
    }
  };

  // -- Validate Webforms -- //
  const $ValidationsScope = {
    // Constructor
    init: function () {
      this.validateWebforms();
    },
    validateWebforms: function () {
      //Add new methods to validate fields
      jQuery.validator.addMethod(
        "emailordomain",
        function (value, element) {
          return (
            this.optional(element) ||
            /^\w+@[a-zA-Z_]+?\.[a-zA-Z]{2,3}$/.test(value) ||
            /[-a-zA-Z0-9@:%_\+.~#?&//=]{2,256}\.[a-z]{2,4}\b(\/[-a-zA-Z0-9@:%_\+.~#?&//=]*)?/.test(
              value
            )
          );
        },
        "Please specify the correct url/email"
      );

      jQuery.validator.addMethod(
        "lettersonly",
        function (value, element) {
          return (
            this.optional(element) ||
            /^[A-Za-z\u00C0-\u017F" "ñ]+$/i.test(value)
          );
        },
        "Letters and spaces only please"
      );

      let RegisterEventsForm = $("#webform-client-form-71");


      if (RegisterEventsForm.length > 0) {
        // jQuery validate
        RegisterEventsForm.validate({
          ignore: [],
          rules: {
            "submitted[name]": {
              required: true,
              minlength: 3,
              lettersonly: true
            },
            "submitted[surname]": {
              required: true,
              minlength: 3,
              lettersonly: true
            },
            "submitted[birthday][month]": {
              required: true,
            },
            "submitted[birthday][day]": {
              required: true,
            },
            "submitted[birthday][year]": {
              required: true,
            },
            "submitted[id_number]": {
              required: true,
              minlength: 6,
              maxlength: 13,
              number: true
            },
            "submitted[email]": {
              required: true,
              email: true,
              emailordomain: true
            },
            "submitted[cellphone]": {
              required: true,
              minlength: 6,
              maxlength: 10,
              number: true
            },
            "submitted[province]": {
              required: true,
              minlength: 3,
              lettersonly: true
            },
            "submitted[city]": {
              required: true,
              minlength: 3,
              lettersonly: true
            },
            "submitted[sneakername]": {
              required: true,
              maxlength: 8,
              lettersonly: true
            },
            "submitted[address]": {
              required: true,
              minlength: 3,
              lettersonly: true
            },
            "submitted[shoesize]": {
              required: true
            },
            "submitted[sneaker]": {
              required: true
            },
            "submitted[informacion_adicional]": {
              required: true,
              minlength: 3
            },
            "submitted[checkboxes][terms]": {
              required: true
            }
          },
          errorPlacement: function () {
            return false;
          }
        });
      }

      if ($("#webform-client-form-71 .webform-component--email").hasClass('error-processed')) {
        $("#edit-submitted-email").after('<label id="edit-submitted-email-error-back" class="error" for="edit-submitted-email">This email already exists.</label>');

        setTimeout(function () {
          // replicate entred values
          if ($("#edit-submitted-sneaker-1").is(":checked")) {
            $(".snick1").click();
          } else if ($("#edit-submitted-sneaker-2").is(":checked")) {
            $(".snick2").click();
          } else if ($("#edit-submitted-sneaker-3").is(":checked")) {
            $(".snick3").click();
          }
          $('#nameinputown').text($('#edit-submitted-sneakername').val());
          $('.reflect').text($('#edit-submitted-sneakername').val());
        }, 300);

        $("#edit-submitted-email").change(function () {
          $("#edit-submitted-email-error-back").hide();
        });
      } else if (!$(".webform-component--email").hasClass('error-processed') && $("#edit-submitted-email").after().is("#edit-submitted-email-error-back")) {
        $("#edit-submitted-email-error-back").hide();
      }

      $('.form-actions .webform-submit').click(function (event) {
        console.log('SUBMIT BUTTON');
        if ($('#nameinputown').val().length === 0) {
          $("#caracter").addClass("sho");
        }
        if ($('input[type=radio]:unchecked').length === 3) {
          $('#choosenick').addClass('shor');
        } else if ($('input[type=radio]:unchecked').length < 3) {
          $('#choosenick').removeClass('shor');
        }
      });

      $(".snick1").click(function () {
        $("#edit-submitted-sneaker-1").prop("checked", true);
        $(".snickimage").hide();
        $("figure#nick1").show();
        $(".snickimage.backker").hide();
        $('#choosenick').removeClass('shor');
      });
      $(".snick2").click(function () {
        $("#edit-submitted-sneaker-2").prop("checked", true);
        $(".snickimage").hide();
        $("figure#nick2").show();
        $(".snickimage.backker").hide();
        $('#choosenick').removeClass('shor');
      });
      $(".snick3").click(function () {
        $("#edit-submitted-sneaker-3").prop("checked", true);
        $(".snickimage").hide();
        $("figure#nick3").show();
        $(".snickimage.backker").hide();
        $('#choosenick').removeClass('shor');
      });

      $('#nameinputown').keyup(function () {
        $('.reflect').text($(this).val());
        $('#edit-submitted-sneakername').val($(this).val());
        if ($('#caracter').hasClass("sho")) {
          $("#caracter").removeClass("sho");
        }
      });

      if (window.location.search.indexOf('submit=done') > -1) {
        $('.lightboxsneak').show();
      }

      RegisterEventsForm.submit(function (event) {
        // console.log('SUBMITED PREV');

        var sneaker = $('form input[type=radio]:checked').val();
        var namesneaker = $('#nameinputown').val();

        if (RegisterEventsForm.valid()) {
          // console.log('SUBMITED completed');
          dataLayer.push({
            event: "Sneaker Selector Register Form",
            eventCategory: "Sneaker Selector Register Form",
            eventAction: "Click",
            eventLabel: "Submit",
            eventValue: '_VALUE_'

          });

          dataLayer.push({
            event: 'trackEvent',
            eventCategory: 'Sneaker Selected',
            eventAction: sneaker,
            eventLabel: namesneaker,
            eventValue: '_VALUE_'

          });
        }
      });

      let ContactForm = $("#webform-client-form-6");

      if (ContactForm.length > 0) {
        // jQuery ContactForm
        ContactForm.validate({
          rules: {
            "submitted[name]": {
              required: true,
              minlength: 3,
              lettersonly: true
            },
            "submitted[surname]": {
              required: true,
              minlength: 3,
              lettersonly: true
            },
            "submitted[your_message]": {
              required: true,
              minlength: 3
            }
          },
          errorPlacement: function () {
            return false;
          }
        });
      }

      ContactForm.submit(function (event) {
        if (ContactForm.valid()) {
          dataLayer.push({
            event: "Contact",
            eventCategory: "Contact",
            eventAction: "Sent",
            eventLabel: $("#edit-submitted-name").val()
          });
        }
      });
    }
  };

  // -- Blog -- //
  const $BlogScope = {
    // Constructor
    init: function () {
      // this.blogGalleryScripts();
      this.blogSlider();
      this.blogDetailSlider();
    },

    // scripts for Agegate
    blogSlider: function () {
      let slider_wrapper = $(".view-blog-list .view-content");
      let slider_items = slider_wrapper.find(".container-blog-slideshow");
      let slick_settings = {
        arrows: false,
        dots: true,
        infinite: false,
        slidesToShow: 1,
        slidesToScroll: 1,
        responsive: [
          {
            breakpoint: 1028,
            settings: {
              speed: 300,
              slidesToScroll: 1,
              slidesToShow: 1,
              arrows: false,
              dots: true,
              centerMode: true,
              centerPadding: "14px"
            }
          }
        ]
      };

      if (!slider_wrapper.hasClass("slick-initialized")) {
        const slick_slider = slider_wrapper.slick(slick_settings);
      }

      // if(slider_items){
      // 	slider_items.each(function(index, el) {
      // 		let instance = $(this);
      // 		let titleItem = instance.find('.icon-pager');
      // 		let titleColor = titleItem.data('color');

      // 		if(instance.hasClass('slick-current')) {
      // 			// Add color in span icon
      // 			titleItem.css('background-color','#'+titleColor);
      // 			instance.find('h4').css('color','#'+titleColor);
      // 		} else {
      // 			titleItem.css('background-color','#002f73');
      // 		}

      // 		// Actice click
      // 		if($(window).width() > 768) {
      // 			instance.click(function(event) {
      // 				/* Act on the event */
      // 				slider_items.removeClass('slick-current');
      // 				instance.addClass('slick-current');

      // 				slider_items.find('.icon-pager').css('background-color','#002f73');

      // 				if(instance.hasClass('slick-current')) {
      // 					// Add color in span icon
      // 					titleItem.css('background-color','#'+titleColor);
      // 					instance.find('h4').css('color','#'+titleColor);
      // 				}
      // 			});
      // 		}
      // 	});
      // }
    },
    blogDetailSlider: function () {
      let slider_wrapper = $(".slide-blog-images");
      let slider_items = slider_wrapper.find(".container-blog-slideshow");
      let slick_settings = {
        arrows: false,
        dots: true,
        infinite: false,
        slidesToShow: 1,
        slidesToScroll: 1,
        adaptiveHeight: true,
        responsive: [
          {
            breakpoint: 1028,
            settings: {
              speed: 300,
              slidesToScroll: 1,
              slidesToShow: 1,
              arrows: false,
              dots: true,
              centerMode: true
              // centerPadding: '14px',
            }
          }
        ]
      };

      if (!slider_wrapper.hasClass("slick-initialized")) {
        const slick_slider = slider_wrapper.slick(slick_settings);
      }
    },

    // scripts for Blog
    blogGalleryScripts: function () {
      $(".view-content .gallery-items").hide();
      $(".blog-see-more").click(function (event) {
        event.preventDefault();
        var gallery_images = $(this)
          .parents(".views-row")
          .find(".gallery-items")
          .clone();
        var parentItem = $(this).parents(".views-row");
        $(".blog-gallery-images-show").html(gallery_images);
        $(".view-footer .gallery-items .gallery-item").each(function (
          index,
          element
        ) {
          $(this).html(
            '<img src="' + $(this).text() + '" alt="image-gallery">'
          );
        });
        $(".view-footer .gallery-items").show();

        $(".view-blog-list .view-content .views-row").removeClass(
          "item-active"
        );
        parentItem.addClass("item-active");
      });
    }
  };

  // -- Single video page -- //
  const $flavourchillas = {
    // Constructor
    init: function () {
      this.buildVideo();
    },

    // Methods
    buildVideo: function () {
      let videoSource = $("body").data("bg-youtube-video-id");

      if (videoSource != "") {
        let MarkupVideo = `<div class="hero-video">
					<iframe width="100%" height="315" src="https://www.youtube.com/embed/${videoSource}?autoplay=1&loop=1&rel=0&playlist=${videoSource}" frameborder="0" scrolling="yes" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
				</div>`;

        $("#block-system-main").prepend(MarkupVideo);
      }
    }
  };

  // ----------------------------
  // TRIGGERS
  // ----------------------------

  // General
  $GeneralScope.init();

  // Agegate
  $AgegateScope.init();

  // Front scripts 
  if ($("body").hasClass("front")) {
    $HomeScope.init();
  }

  // Front scripts 
  if ($("body").hasClass("wtff")) {
    $WtfScope.init();
  }

  // About scripts
  if ($("body").hasClass("custom-flying-fish-about")) {
    $AboutScope.init();
  }

  // Events scripts
  if ($("body").hasClass("custom-flying-fish-events")) {
    $EventsScope.init();
  }
  
  // WhatsAppPage scripts
  if ($("body").hasClass("custom-flying-fish-whatsapp")) {
    $WhatsScope.init();
  }

  // flavourchillas page scripts
  if ($("body").hasClass("flavourchillas")) {
    $flavourchillas.init();
  }

  // RegisterEventsPage scripts
  if ($("body").hasClass("custom-flying-fish-register-events")) {
    $ValidationsScope.init();
  }

  if ($("body").hasClass("snick")) {
    $ValidationsScope.init();
  }

  if ($("body").hasClass("flow-room-drops")) {
    $ValidationsScope.init();
  }

  // ContactPage scripts
  if ($("body").hasClass("custom-flying-fish-contact-us")) {
    $ValidationsScope.init();
  }

  // BlogPage scripts
  if ($("body").hasClass("custom-flying-fish-blog")) {
    $BlogScope.init();
  }

  $('#fbpuzzle').bind("click", function () {
    var text = "You rock for solving the puzzle, but don’t be so quick to leave The ClubHouse, stick around, pop into our online shop and see how you can Add Some Flavour to your life.";
    var pageUrl = window.location.origin;
    window.open('https://www.facebook.com/sharer/sharer.php?u=' + encodeURIComponent(pageUrl) + '&quote=' + encodeURIComponent(text));
  });

  $('#twitterpuzzle').bind("click", function () {
    var text = "You rock for solving the puzzle, but don’t be so quick to leave The ClubHouse, stick around, pop into our online shop and see how you can Add Some Flavour to your life.";
    var pageUrl = window.location.origin;
    window.open('https://twitter.com/share?url=' + encodeURIComponent(pageUrl) + '&text=' + encodeURIComponent(text), '', 'left=0,top=0,width=550,height=450,personalbar=0,toolbar=0,scrollbars=0,resizable=0');
  });
});
